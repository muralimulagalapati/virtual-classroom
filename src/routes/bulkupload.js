'use strict'

const getResponse = require('../lib/responsecodes')
const { verifyFileListExists, copyFileList } = require('../service/s3operations')
const { getContentKey } = require('../lib/createKeys')
const { updateContent } = require('../models/content')
const { getContentMetadata } = require('../lib/generateMetadata')
const { createETag } = require('../models/etags')

const copyAndUpdateFiles = async (contentKey, filelist) => {
  try {
    const filenameList = filelist.map(file => file.filename)
    const newFilenameList = await copyFileList(filenameList)
    const files = filelist.map((file, index) => ({
      originalFilename: file.filename,
      newFilename: newFilenameList[index],
      topicName: file.topicName,
      topicNumber: file.topicNumber,
      contentType: file.contentType
    }))
    const metadatalist = files.map(getContentMetadata)
    await updateContent(contentKey, metadatalist)
    await createETag('content')
  } catch(ex) {
    console.error(ex)
    throw ex
  }
}

const getNotFoundFiles = (results, filenameList) => {
  let notFoundFiles = results.filter(result => (
    !result.fileExists
  ))
  notFoundFiles = notFoundFiles.map(file => file.filename)

  if (Array.isArray(notFoundFiles) && notFoundFiles.length) {
    notFoundFiles = filenameList.filter(filename => (
      notFoundFiles.includes(filename)
    ))
  }
  return notFoundFiles
}

const bulkUpload = async (req, res) => {

  const [error, contentKey] = getContentKey(req.body)
  if (error) {
    console.error(error)
    return res.status(400).json(getResponse(false, ''))
  }

  let { filelist } = req.body
  if (!Array.isArray(filelist) || !filelist.length) {
    return res.status(400).json(getResponse(false, ''))
  }

  filelist = filelist.map(file => ({
    filename: file["topic.type.uri"],
    topicName: file["topic.name"],
    topicNumber: file["topic.number"],
    contentType: file["topic.type.contentType"]
  }))
  let filenameList = filelist.map(file => file.filename)

  try {
    const results = await verifyFileListExists(filenameList)
    const notFoundFiles = getNotFoundFiles(results, filenameList)
    if (notFoundFiles.length) {
      throw { missingFiles: notFoundFiles }
    }
    res.status(200).json(getResponse(true, 'Record created successfully'))
    try {
      await copyAndUpdateFiles(contentKey, filelist)
    } catch (ex) {
      console.error("error copying files", ex)
    }
  } catch(ex) {
    if (ex.missingFiles) {
      return res.status(404).json({
        success: false,
        missingFiles: ex.missingFiles
      })
    }
    console.error(ex)
    res.status(500).json(getResponse(false, 'Error connecting to S3'))
  }
}

module.exports = {
  bulkUpload
}
