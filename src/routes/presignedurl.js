'use strict'

const getResponse = require('../lib/responsecodes')
const { getSignedUrl } = require('../service/s3operations')

module.exports = (req, res) => {
  const { filename } = req.query
  if (!filename) {
    console.error('Invalid parameter filename')
    return res.status(400).json(getResponse(false, 'filename cannot be empty'))
  }
  
  try {
    const response = getSignedUrl(filename)
    res.status(200).json(response)
  } catch(ex) {
    console.error(ex)
    res.status(500).json(getResponse(false, 'Error getting presignedurl')) 
  }
}
