'use strict'

const uuidv1 = require('uuid/v1')

const CONTENT_BUCKET = process.env.CONTENT_BUCKET

const getContentMetadata = (file) => {
  let ext = file.originalFilename.match(/\.(\w+$)/)
  ext = ext.length > 1 ? ext[1] : 'NA'

  return {
    contentId: uuidv1(),
    contentName: file.topicName,
    contentResourceType: file.contentType,
    contentStorageKey: file.newFilename,
    contentStorageBucketName: CONTENT_BUCKET,
    contentFileName: file.originalFilename,
    contentFileType: ext
  }
}

module.exports = {
  getContentMetadata
}
